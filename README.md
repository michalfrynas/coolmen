0. Build project:
> ```make GNU_INSTALL_ROOT=~/Devel/Coolmen/gcc-arm-none-eabi-7-2018-q2-update```  
> ```make GNU_INSTALL_ROOT=~/Devel/Coolmen/gcc-arm-none-eabi-7-2018-q2-update clean```  

1. Download and install into ~/tmp [gcc compiler suite](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
2. Download and install into ~/tmp [nRF51 SDK](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v12.x.x/nRF5_SDK_12.3.0_d7731ad.zip)

3. Download coolmen source code (for now it's tarball, later on gitlab/github)
> ```cd ~/tmp/coolmen-pulseoximeter```

4. Build and / or clean
>```make GNU_INSTALL_ROOT=~/tmp/gcc-arm-none-eabi-8-2018-q4-major```  
>```make GNU_INSTALL_ROOT=~/tmp/gcc-arm-none-eabi-8-2018-q4-major clean```  
  
5. Install openocd
>```sudo apt install openocd (version 0.10.0-4)```
  
6. Connect J-Link and start openocd
>```openocd -f  ~/tmp/coolmen-pulseoximeter/scripts/coolmen.cfg```
  
7. Flash softdevice and application binary
>```~/tmp/coolmen-pulseoximeter/scripts/flash.pl ~/tmp/nRF5_SDK_12.3.0_d7731ad/components/softdevice/s130/hex/s130_nrf51_2.0.1_softdevice.hex 0```  
>```~/tmp/coolmen-pulseoximeter/scripts/flash.pl ~/tmp/coolmen-pulseoximeter/build/coolmen_xxac.hex 0x1B000```  
>```~/tmp/coolmen-pulseoximeter/scripts/reset.pl```  
  
8. Debug with GDB
> ```~/tmp/gcc-arm-none-eabi-8-2018-q4-major/bin/arm-none-eabi-gdb ~/tmp/coolmen-pulseoximeter/build/coolmen_xxac.out```  
> ```gdb> target remote localhost:3333```  
> ```gdb> load```  
> ```gdb> break main```  
> ```gdb> break main.c:60```  
> ```gdb> cont```  
