#!/usr/bin/perl
# requires: libnet-telnet-perl

use Net::Telnet;

#$numArgs = $#ARGV + 1;
#if($numArgs != 1) {
#    die("Usage ./erase.pl [all|app] \n");
#}

#$target   = $ARGV[0];

$ip = "127.0.0.1";
$port = 4444;

$telnet = new Net::Telnet (
    Port   => $port,
    Timeout=>10,
    Errmode=>'die',
    Prompt =>'/>/');

$telnet->open($ip);

print $telnet->cmd('init');
print $telnet->cmd('reset halt');
print $telnet->cmd('nrf51 mass_erase');
#if($target eq "all") {
#	print $telnet->cmd('stm32f1x mass_erase 0');
#}
#elsif($target eq "app") {
#	print $telnet->cmd('flash erase_sector 0 64 255');
#}
#else {
#	die("Usage ./erase.pl [all|app] \n");
#}
print $telnet->cmd('reset halt');
print $telnet->cmd('exit');

print "\n";
