#!/usr/bin/perl
# requires: libnet-telnet-perl

use Net::Telnet;

$ip = "127.0.0.1";
$port = 4444;

$telnet = new Net::Telnet (
    Port   => $port,
    Timeout=>10,
    Errmode=>'die',
    Prompt =>'/>/');

$telnet->open($ip);

print $telnet->cmd('reset run');
print $telnet->cmd('exit');

print "\n";
