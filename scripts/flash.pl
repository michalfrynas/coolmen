#!/usr/bin/perl
# requires: libnet-telnet-perl

use Net::Telnet;
use File::Spec;

$numArgs = $#ARGV + 1;
if($numArgs != 2) {
    die( "Usage ./flash.pl [file.bin] [address]\n");
}

$file = $ARGV[0];
$file_abs_path = File::Spec->rel2abs($file);
$address = $ARGV[1];

$ip = "127.0.0.1";
$port = 4444;

$telnet = new Net::Telnet (
    Port   => $port,
    Timeout=>50,
    Errmode=>'die',
    Prompt =>'/>/');

$telnet->open($ip);

print $telnet->cmd('init');
print $telnet->cmd('reset halt');
print $telnet->cmd('flash probe 0');
print $telnet->cmd('flash write_image erase '.$file_abs_path.' '.$address);
print $telnet->cmd('reset');
print $telnet->cmd('exit');

print "\n";
