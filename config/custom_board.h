#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

#define LEDS_NUMBER    2

#define LED_START      8
#define LED_1          8
#define LED_2          26
#define LED_STOP       26

#define LEDS_ACTIVE_STATE 0

#define LEDS_LIST { LED_1, LED_2 }

#define LEDS_INV_MASK  LEDS_MASK

#define BSP_LED_0      LED_1

#define TWI_MASTER_CONFIG_CLOCK_PIN_NUMBER 		(1U)
#define TWI_MASTER_CONFIG_DATA_PIN_NUMBER 		(0U)

#endif // CUSTOM_BOARD
