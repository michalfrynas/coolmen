#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"
#include "pulseoximeter/twi_master.h"
#include "pulseoximeter/max30101.h"

#define NRF_LOG_MODULE_NAME " coolmen"

#include "log/nrf_log.h"
#include "log/nrf_log_ctrl.h"

/**
 * @brief Function for application main entry.
 */
int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_RAW_INFO("\n\n============================= Coolmen application =============================\n\n\r");
    NRF_LOG_FLUSH();

	/* Configure board. */
	bsp_board_leds_init();

	nrf_delay_ms(100);

	// Initialize TWI bus
	twi_master_init();

	max30101_init(MultiLedMode);

	while (true)
	{
		for (int i = 0; i < LEDS_NUMBER; i++)
		{
//			uint8_t data[288];
//			uint16_t read = 0;
//			memset(data, 0, 288);
//			uint8_t lost = 0;
//			max30101_twi_read(OverflowCounter, &lost);
//			max30101_readDataFifo(data, &read);
//			NRF_LOG_RAW_INFO("[%d] ", read);
//			int j = 0;
//			for (uint16_t i = 0; i < read; i += 3) {
//			                uint8_t sample[4] = {0};
//			                sample[0] = data[i + 2];
//			                sample[1] = data[i + 1];
//			                sample[2] = data[i];
////			                NRF_LOG_RAW_INFO("%5d ", *(uint32_t *) sample);
////			                NRF_LOG_FLUSH();
//			                j++;
//			                if (j == 3){
//			                	j = 0;
////			                	NRF_LOG_RAW_INFO("\r\n");
//			                }
//			}
//			dupa += read;
			NRF_LOG_RAW_INFO("\r\n");
//			NRF_LOG_INFO("diff_50ms = %d, samples = %d, diff = %d, lost = %d\r\n",
//					cnt++, dupa, read, lost);
			NRF_LOG_FLUSH();
			nrf_delay_ms(100);
			bsp_board_led_invert(i);
			static uint8_t dupa = 0;
			dupa = (dupa == 0 ? 0x50 : 0);
			max30101_setLedPulseAmplitude(LED1_PA, dupa);
			nrf_delay_ms(100);
//			max30101_shutdown();
		}
	}
}
