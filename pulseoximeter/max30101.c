#include <stdbool.h>
#include "boards.h"
#include "app_error.h"
#include "max30101.h"
#include "pulseoximeter/twi_master.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_timer.h"
#include "nrf_delay.h"

#define NRF_LOG_MODULE_NAME "max30101"

#include "log/nrf_log.h"
#include "log/nrf_log_ctrl.h"

#define MAX30101_TWI_ADDRESS	(0xAE)

const nrf_drv_timer_t TIMER_INSTANCE = NRF_DRV_TIMER_INSTANCE(0);
static const uint8_t BYTES_PER_CHANNEL = 3;
static LedChannels LED_CHANNELS;
static uint8_t SAMPLE_SIZE_BYTES;

void max30101_timer_init();
void max30101_timer_handler(nrf_timer_event_t event_type, void* p_context);


void max30101_init(OperationModes operationalMode)
{
//	max30101_timer_init();

	max30101_reset();
	nrf_delay_ms(10);

	ModeConfiguration_reg modeConfigurationRegister;
	modeConfigurationRegister.all = 0;
	modeConfigurationRegister.bits.mode = operationalMode;
	max30101_setModeConfiguration(modeConfigurationRegister);

	FifoConfiguration_reg fifoConfigurationRegister;
	fifoConfigurationRegister.bits.fifo_roll_over_en = 1; // Roll FIFO over on fulll
	fifoConfigurationRegister.bits.sample_average = AveragedSamples_0; // Number of averaged samples
	fifoConfigurationRegister.bits.fifo_a_full = 0; // FIFO almost full interrupt
	max30101_setFifoConfiguration(fifoConfigurationRegister);

	SpO2Configuration_reg spo2ConfigurationRegister;
	spo2ConfigurationRegister.bits.spo2_adc_range = ADC_Range_3;
	spo2ConfigurationRegister.bits.led_pw = PW_3;	// pulse width 411us, 18 bit ADC
	spo2ConfigurationRegister.bits.spo2_sr = SR_100_Hz; // 100 samples per second
	max30101_setSpO2Configuration(spo2ConfigurationRegister);

	ModeControl_reg modeControlRegister;

	switch (operationalMode)
	{
		case HeartRateMode:
			LED_CHANNELS = OneLedChannel;
			SAMPLE_SIZE_BYTES = 3;

			max30101_setLedPulseAmplitude(LED1_PA, 0x05);
			break;

		case SpO2Mode:
			LED_CHANNELS = TwoLedChannels;
			SAMPLE_SIZE_BYTES = 6;

			max30101_setLedPulseAmplitude(LED1_PA, 0x05);
			max30101_setLedPulseAmplitude(LED2_PA, 0x05);
			break;

		case MultiLedMode:
		default:
			LED_CHANNELS = ThreeLedChannels;
			SAMPLE_SIZE_BYTES = 9;

			max30101_setLedPulseAmplitude(LED1_PA, 0x05); // 0.10 mA
			max30101_setLedPulseAmplitude(LED2_PA, 0x05); // 0.10 mA
			max30101_setLedPulseAmplitude(LED3_PA, 0x05); // 0.10 mA

			// Red Led on slot 1, IR Led on slot2
			modeControlRegister.bits.hi_slot = 2;
			modeControlRegister.bits.lo_slot = 1;
			max30101_setMultiLedModeControl(ModeControlReg1, modeControlRegister);

			// Green Led on slot 3
			modeControlRegister.bits.hi_slot = 0;
			modeControlRegister.bits.lo_slot = 3;
			max30101_setMultiLedModeControl(ModeControlReg2, modeControlRegister);

			break;
	}

	// The FIFO write/read pointers should be cleared (back to 0x00) upon entering SpO2
	// mode or HR mode, so that there is no old data represented in the FIFO.
	max30101_twi_write(FIFO_ReadPointer, 0x00);
	max30101_twi_write(FIFO_WritePointer, 0x00);
}

void max30101_reset()
{
	ModeConfiguration_reg modeConfigurationRegister;
	modeConfigurationRegister.all = 0;
	modeConfigurationRegister.bits.reset = 1;
	max30101_setModeConfiguration(modeConfigurationRegister);
}

void max30101_shutdown()
{
	ModeConfiguration_reg modeConfigurationRegister;
	modeConfigurationRegister.all = 0;
	modeConfigurationRegister.bits.shdn = 1;
	max30101_setModeConfiguration(modeConfigurationRegister);
}

void max30101_readDataFifo(uint8_t *data, uint16_t *readBytes)
{
	uint8_t fifoWritePtr = 0x00;
	uint8_t fifoReadPtr = 0x00;
	uint16_t fifoNumBytes = 0;
	*readBytes = 0;

	// Get FIFO write and read pointers
	if (max30101_twi_read(FIFO_WritePointer, &fifoWritePtr)
		&& max30101_twi_read(FIFO_ReadPointer, &fifoReadPtr))
	{
		// Calculate size of data to read
        if (fifoWritePtr > fifoReadPtr)
        {
            fifoNumBytes = ((fifoWritePtr - fifoReadPtr) *
                              (BYTES_PER_CHANNEL * LED_CHANNELS));
        }
        else
        {
            fifoNumBytes = (((32 - fifoReadPtr) + fifoWritePtr) *
                               (BYTES_PER_CHANNEL * LED_CHANNELS));
        }

		uint8_t local_data[288];
		local_data[0] = FIFO_DataRegister;

        if (twi_master_transfer(MAX30101_TWI_ADDRESS, local_data, 1, TWI_DONT_ISSUE_STOP)
            && twi_master_transfer(MAX30101_TWI_ADDRESS | TWI_READ_BIT, local_data, fifoNumBytes, TWI_DONT_ISSUE_STOP))
        {
            memcpy(data, local_data, fifoNumBytes);
            *readBytes = fifoNumBytes;
        }
	}
}





bool max30101_setFifoConfiguration(const FifoConfiguration_reg config)
{
	return max30101_twi_write(FIFO_Configuration, config.all);
}

bool max30101_setModeConfiguration(const ModeConfiguration_reg config)
{
	return max30101_twi_write(ModeConfiguration, (config.all & 0xC7));
}

bool max30101_setSpO2Configuration(const SpO2Configuration_reg config)
{
	return max30101_twi_write(SpO2Configuration, (config.all & 0x7F));
}

bool max30101_setLedPulseAmplitude(Registers reg, const uint8_t amplitude)
{
	return max30101_twi_write(reg, amplitude);
}

bool max30101_setMultiLedModeControl(Registers reg, const ModeControl_reg data)
{
	return max30101_twi_write(reg, data.all);
}

void max30101_timer_init()
{
    uint32_t err_code = NRF_SUCCESS;
    uint32_t time_ms = 100; // Time (in miliseconds) between consecutive compare events.
    uint32_t time_ticks;

    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    err_code = nrf_drv_timer_init(&TIMER_INSTANCE, &timer_cfg, max30101_timer_handler);
    APP_ERROR_CHECK(err_code);

    time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_INSTANCE, time_ms);

    nrf_drv_timer_extended_compare(
         &TIMER_INSTANCE, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

    nrf_drv_timer_enable(&TIMER_INSTANCE);
}

/**
 * @brief Handler for timer events.
 */
void max30101_timer_handler(nrf_timer_event_t event_type, void* p_context)
{
    static uint32_t i;
    uint32_t led_to_invert = ((i++) % LEDS_NUMBER);

    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
            bsp_board_led_invert(led_to_invert);
            break;

        default:
            //Do nothing.
            break;
    }
}

bool max30101_twi_read(uint8_t u8register_address, uint8_t *pu8value)
{
	bool transfer_succeeded = false;

	transfer_succeeded = twi_master_transfer(MAX30101_TWI_ADDRESS, &u8register_address, 1, TWI_DONT_ISSUE_STOP);
	APP_ERROR_CHECK_BOOL(transfer_succeeded);

	transfer_succeeded = twi_master_transfer(MAX30101_TWI_ADDRESS | TWI_READ_BIT, pu8value, 1, TWI_DONT_ISSUE_STOP);
	APP_ERROR_CHECK_BOOL(transfer_succeeded);

	return transfer_succeeded;
}

bool max30101_twi_write(uint8_t u8register_address, uint8_t u8value)
{
	bool transfer_succeeded = false;
	uint8_t u8SendMsg[2];

	u8SendMsg[0] = u8register_address;
	u8SendMsg[1] = u8value;
	transfer_succeeded = twi_master_transfer(MAX30101_TWI_ADDRESS, u8SendMsg, 2, TWI_DONT_ISSUE_STOP);

	return transfer_succeeded;
}
