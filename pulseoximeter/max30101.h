#ifndef MAX30101_H_
#define MAX30101_H_

#include <stdbool.h>
#include <stdint.h>

// MAX30101 Register Map
typedef enum
{
	FIFO_WritePointer  = 0x04,
	OverflowCounter    = 0x05,
	FIFO_ReadPointer   = 0x06,
	FIFO_DataRegister  = 0x07,
	FIFO_Configuration = 0x08,
	ModeConfiguration  = 0x09,
	SpO2Configuration  = 0x0A,
	LED1_PA            = 0x0C,
	LED2_PA            = 0x0D,
	LED3_PA            = 0x0E,
	LED4_PA            = 0x0F,
	ModeControlReg1    = 0x11,
	ModeControlReg2    = 0x12,
	DieTempInt         = 0x1F,
	DieTempFrac        = 0x20,
	DieTempConfig      = 0x21,
	ProxIntThreshold   = 0x30,
	RevID              = 0xFE,
	PartID             = 0xFF
} Registers;

// MAX30101 Operational Modes
typedef enum
{
	HeartRateMode = 2,
	SpO2Mode      = 3,
	MultiLedMode  = 7
} OperationModes;

// Number of LED channels used
typedef enum
{
    OneLedChannel    = 1,
    TwoLedChannels   = 2,
    ThreeLedChannels = 3
} LedChannels;

// Number of samples averaged per FIFO sample, set in FIFO config
typedef enum
{
    AveragedSamples_0  = 0,
    AveragedSamples_2  = 1,
    AveragedSamples_4  = 2,
    AveragedSamples_8  = 3,
    AveragedSamples_16 = 4,
    AveragedSamples_32 = 5
} NumSamplesAveraged;

// ADC Range, set in SpO2 config
enum
{
    ADC_Range_0 = 0,
    ADC_Range_1 = 1,
    ADC_Range_2 = 2,
    ADC_Range_3 = 3
} AdcRange;

// LED PulseWidth, set in SpO2 config
enum
{
    PW_0 = 0,	// 69 us, 15 bit ADC
    PW_1 = 1,	// 118 us, 16 bit ADC
    PW_2 = 2,	// 215 us, 17 bit ADC
    PW_3 = 3	// 411 us, 18 bit ADC
} LedPulseWidth;


// Sample rate, set in SpO2 config
enum
{
    SR_50_Hz   = 0,
    SR_100_Hz  = 1,
    SR_200_Hz  = 2,
    SR_400_Hz  = 3,
    SR_800_Hz  = 4,
    SR_1000_Hz = 5,
    SR_1600_Hz = 6,
    SR_3200_Hz = 7
} SampleRate;


// FIFO Configuration bit fields
typedef union
{
    uint8_t all;
    struct
    {
        uint8_t fifo_a_full       : 4;
        uint8_t fifo_roll_over_en : 1;
        uint8_t sample_average    : 3;
    } bits;
}  FifoConfiguration_reg;

// Mode Configuration bit fields
typedef union
{
	uint8_t all;
	struct
	{
		uint8_t mode     : 3;
		uint8_t reserved : 3;
		uint8_t reset    : 1;
		uint8_t shdn     : 1;
	} bits;
} ModeConfiguration_reg;

// SpO2 Configuration bit fields
typedef union
{
    uint8_t all;
    struct
    {
        uint8_t led_pw         : 2;
        uint8_t spo2_sr        : 3;
        uint8_t spo2_adc_range : 2;
        uint8_t reserved       : 1;
    } bits;
}  SpO2Configuration_reg;

typedef union
{
    uint8_t all;
    struct
    {
	    uint8_t lo_slot     : 3;
	    uint8_t reserved1 	: 1;
	    uint8_t hi_slot     : 3;
	    uint8_t reserved2 	: 1;
    } bits;
} ModeControl_reg;


void max30101_init(OperationModes operationalMode);

void max30101_reset();
void max30101_shutdown();
void max30101_readDataFifo(uint8_t *data, uint16_t *readBytes);

bool max30101_setFifoConfiguration(const FifoConfiguration_reg config);
bool max30101_setModeConfiguration(const ModeConfiguration_reg config);
bool max30101_setSpO2Configuration(const SpO2Configuration_reg config);
bool max30101_setLedPulseAmplitude(Registers reg, const uint8_t amplitude);
bool max30101_setMultiLedModeControl(Registers reg, const ModeControl_reg data);

bool max30101_twi_read(uint8_t u8register_address, uint8_t *pu8value);
bool max30101_twi_write(uint8_t u8register_address, uint8_t u8value);

#endif /* MAX30101_H_ */
